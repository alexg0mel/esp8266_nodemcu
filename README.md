# games with esp


## ext links:

    https://github.com/nodemcu/nodemcu-firmware
    https://nodemcu.readthedocs.io/en/master/

    https://www.youtube.com/watch?v=dyG2_dN28hY

    https://www.youtube.com/watch?v=WrTm5QlWnaE
    https://www.youtube.com/watch?v=WD5VUrk_mbI


    https://esphome.io/
    https://www.home-assistant.io/
    
    ESPlorer https://esp8266.ru/esplorer/
    
    
## Micropython
    http://micropython.org/
    Relp https://docs.micropython.org/en/latest/esp8266/tutorial/repl.html
    https://habr.com/ru/post/341716/
    https://github.com/rdehuyss/micropython-ota-updater
    https://micropython-on-esp8266-workshop.readthedocs.io/en/latest/index.html
    https://github.com/micropython/micropython-lib
    
    
## MQTT
    https://www.hivemq.com/blog/mqtt-toolbox-mqtt-client-chrome-app/
