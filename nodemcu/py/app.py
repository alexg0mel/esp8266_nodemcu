import sys
from machine import Pin
import network
from umqtt.simple import MQTTClient
from time import sleep

p4 = Pin(4, Pin.OUT)
p2 = Pin(2, Pin.OUT)
SID = 'ADM WIFI'
PWD = '80F1083E4B65'
MQTT_HOST = '192.168.20.89'
MQTT_CLIEND_ID = '01:02:03:04:05:06'


def do_connect():
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        print('connecting to network...')
        sta_if.active(True)
        sta_if.connect(SID, PWD)
        while not sta_if.isconnected():
            pass


def callback_routine(feed, msg):
    print('Received Data from: feed={}, msg={}'.format(feed, msg))
    if 'led' in feed:
        action = str(msg, 'utf-8')
        if action == '1':
            p4.on()
        else:
            p4.off()
        print('Action: {}'.format(action))


def main_app():
    ap_if = network.WLAN(network.AP_IF)
    ap_if.active(False)
    do_connect()
    client = MQTTClient(client_id=MQTT_CLIEND_ID, server=MQTT_HOST, port=1883)
    client.connect()
    client.set_callback(callback_routine)
    client.subscribe('led')
    while True:
        try:
            client.wait_msg()
        except KeyboardInterrupt:
            print('Ctrl+C pressed, exiting')
            client.disconnect()
            sys.exit()
